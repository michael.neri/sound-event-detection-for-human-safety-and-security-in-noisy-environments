# -*- coding: utf-8 -*-
"""
    Urban audio classification models using pytorch-lightning
    Michael Neri
    michael.neri@uniroma3.it
"""
import torch
import torch.nn as nn
from pytorch_lightning import LightningModule
from torchlibrosa import LogmelFilterBank, Spectrogram, SpecAugmentation
import torchmetrics
import torch.nn.functional as F
import numpy as np
import torch_audiomentations as ta
from SEDSegmentBasedMetrics import SEDSegmentBasedMetrics

# for init layers 
def init_layer(layer):
    """Initialize a Linear or Convolutional layer. """
    nn.init.xavier_uniform_(layer.weight)
 
    if hasattr(layer, 'bias'):
        if layer.bias is not None:
            layer.bias.data.fill_(0.)
            
    
def init_bn(bn):
    """Initialize a Batchnorm layer. """
    bn.bias.data.fill_(0.)
    bn.weight.data.fill_(1.)

class ConvBlock(nn.Module):
    def __init__(self, in_channels, out_channels, last = False, kernel_conv = None):
        
        super(ConvBlock, self).__init__()
        self.last = last
        if kernel_conv is None:
            self.conv1 = nn.Conv2d(in_channels=in_channels, 
                                out_channels=out_channels,
                                kernel_size=(3, 3), stride=(1, 1),
                                padding=(1, 1), bias=False)
                                
            self.conv2 = nn.Conv2d(in_channels=out_channels, 
                                out_channels=out_channels,
                                kernel_size=(3, 3), stride=(1, 1),
                                padding=(1, 1), bias=False)
        else:
            self.conv1 = nn.Conv2d(in_channels=in_channels, 
                                out_channels=out_channels,
                                kernel_size=kernel_conv,
                                padding='same', bias=False)
                                
            self.conv2 = nn.Conv2d(in_channels=out_channels, 
                                out_channels=out_channels,
                                kernel_size=1, stride=(1, 1),
                                padding='same', bias=False)

                              
        self.bn1 = nn.BatchNorm2d(out_channels)
        self.bn2 = nn.BatchNorm2d(out_channels)

        self.init_weight()
        
    def init_weight(self):
        init_layer(self.conv1)
        init_layer(self.conv2)
        init_bn(self.bn1)
        init_bn(self.bn2)

        
    def forward(self, input, pool_size=(2, 2), pool_type='avg'):
        
        x = input
        x = F.elu(self.bn1(self.conv1(x)))
        if not self.last:
            x = F.elu(self.bn2(self.conv2(x)))
        else:
            x = self.bn2(self.conv2(x))
        if pool_type == 'max':
            x = F.max_pool2d(x, kernel_size=pool_size)
        elif pool_type == 'avg':
            x = F.avg_pool2d(x, kernel_size=pool_size)
        elif pool_type == 'avg+max':
            x1 = F.avg_pool2d(x, kernel_size=pool_size)
            x2 = F.max_pool2d(x, kernel_size=pool_size)
            x = x1 + x2
        else:
            raise Exception('Incorrect argument!')
        
        return x

class ASPPConv(nn.Sequential):
    def __init__(self, in_channels, out_channels, dilation, kernels):

        if kernels[0] > kernels[1]:
            dilation = (dilation, 1)  
        elif kernels[0] < kernels[1]:
            dilation = (1, dilation)
        else:
            pass
        modules = [
            nn.Conv2d(in_channels, out_channels, kernels, padding = 'same', dilation = dilation , bias=False),
            nn.BatchNorm2d(out_channels),
            nn.ELU(),
        ]
        init_layer(modules[0])
        init_bn(modules[1])
        super().__init__(*modules)

class ASPPPooling(nn.Sequential):

    def __init__(self, in_channels, out_channels):
        modules = [            
                nn.AdaptiveAvgPool2d(1),
                nn.Conv2d(in_channels, out_channels, 1, bias=False),
                nn.BatchNorm2d(out_channels),
                nn.ELU(),
            ]
        init_layer(modules[1])
        init_bn(modules[2])
        super().__init__(*modules)

    def forward(self, x):
        size = x.shape[-2:]
        for mod in self:
            x = mod(x)
        return F.interpolate(x, size=size, mode="bilinear", align_corners=False)

class ASPP(nn.Module):

    def __init__(self, in_channels, atrous_rates, out_channels, kernels):
        super().__init__()
        modules = []
        modules.append(
            nn.Sequential(nn.Conv2d(in_channels, out_channels, kernels, padding = 'same', bias=False), nn.BatchNorm2d(out_channels), nn.ELU())
        )
        init_layer(modules[0][0])
        init_bn(modules[0][1])
        rates = tuple(atrous_rates)
        for rate in rates:
            modules.append(ASPPConv(in_channels, out_channels, rate, kernels))

        modules.append(ASPPPooling(in_channels, out_channels))

        self.convs = nn.ModuleList(modules)

        self.project = nn.Sequential(
            nn.Conv2d(len(self.convs) * out_channels, out_channels, 1, bias=False),
            nn.BatchNorm2d(out_channels),
            nn.ELU(),
        )
        init_layer(self.project[0])
        init_bn(self.project[1])

    def forward(self, x):
        _res = []
        for conv in self.convs:
            _res.append(conv(x))
        res = torch.cat(_res, dim=1)
        return self.project(res)

class FirstDerivative(nn.Module):
    
    def __init__(self, nmels = 64):
        super().__init__()
        a = np.array([[-1, 0, 1], [-2, 0, 2], [-1, 0, 1]])
        self.conv1 = torch.nn.Conv2d(1, 1, kernel_size=3, stride=1, padding=1, bias=False)
        self.conv1.weight = torch.nn.Parameter(torch.from_numpy(a).float().unsqueeze(0).unsqueeze(0), requires_grad = False)

        b = np.array([[-1, -2, -1], [0 ,0, 0], [1, 2, 1]])
        self.conv2 = torch.nn.Conv2d(1, 1, kernel_size=3, stride=1, padding=1, bias=False)
        self.conv2.weight = torch.nn.Parameter(torch.from_numpy(b).float().unsqueeze(0).unsqueeze(0), requires_grad = False)


    def forward(self, x):
        G_x = self.conv1(x)
        G_y = self.conv2(x)
        G = torch.sqrt(torch.pow(G_x,2) + torch.pow(G_y,2))

        size_spec = G.size()
        G = G.view(size_spec[0], -1)
        G -= G.min(1, keepdim=True)[0]
        G /= G.max(1, keepdim=True)[0]
        G = G.view(size_spec)
        # G = G.transpose(1,3)
        # G = self.bn(G)
        # G = G.transpose(1,3)
        return G

class SecondDerative(nn.Module):
    def __init__(self, nmels = 64):
        super().__init__()
        a = np.array([[0, 1, 0], [1, -4, 1], [0,1,0]])
        self.conv1 = torch.nn.Conv2d(1, 1, kernel_size=3, stride=1, padding=1, bias=False)
        self.conv1.weight = torch.nn.Parameter(torch.from_numpy(a).float().unsqueeze(0).unsqueeze(0), requires_grad = False)
        # self.bn = torch.nn.BatchNorm2d(nmels)
        # init_bn(self.bn)

    def forward(self, x):
        G = self.conv1(x)
            
        size_spec = G.size()
        G = G.view(size_spec[0], -1)
        G -= G.min(1, keepdim=True)[0]
        G /= G.max(1, keepdim=True)[0]
        G = G.view(size_spec)
        # G = G.transpose(1,3)
        # G = self.bn(G)
        # G = G.transpose(1,3)
        return G


class AuSPP(LightningModule):
    def __init__(self, sample_rate, window_size, hop_size, mel_bins, fmin, fmax, drop = 0.2, lr = 0.001, num_classes = 50, max_duration = 4, frame_length = 0.02):

        super(AuSPP, self).__init__()

        ### Hyper parameters init
        self.sample_rate = sample_rate
        self.window_size = window_size
        self.hop_size = hop_size
        self.mel_bins = mel_bins
        self.fmin = fmin
        self.fmax = fmax
        self.lr = lr
        self.num_classes = num_classes
        self.drop = drop
        self.max_duration = max_duration
        self.frame_length = frame_length
        self.num_frames = int(self.max_duration/self.frame_length)

        # fixed parameters for spectrogram extractor and mel-filterback
        window = 'hann'
        center = True
        pad_mode = 'reflect'
        ref = 1.0
        amin = 1e-10
        top_db = 80

        self.save_hyperparameters() 

        # Spectrogram extractor
        self.spectrogram_extractor = Spectrogram(n_fft = window_size, hop_length=hop_size, 
            win_length=window_size, window=window, center=center, pad_mode=pad_mode, 
            freeze_parameters=True)

        # Logmel feature extractor
        self.logmel_extractor = LogmelFilterBank(sr=sample_rate, n_fft = window_size, 
            n_mels=mel_bins, fmin=fmin, fmax=fmax, ref=ref, amin=amin, top_db=top_db, 
            freeze_parameters=True)

        self.spectrogram_augmenter = SpecAugmentation(time_drop_width=64, time_stripes_num=2, 
            freq_drop_width=8, freq_stripes_num=2)

        self.waveform_aug = ta.Compose(
            transforms= [
            ta.Gain(
                min_gain_in_db=-15.0,
                max_gain_in_db=5.0,
                p=0.5,
                ),
            ta.PolarityInversion(p = 0.5),
            ta.PitchShift(p = 0.5, sample_rate = self.sample_rate),
            ta.AddColoredNoise(p = 0.5),
        ]
        )

        self.spectrogram_norm = nn.BatchNorm2d(self.mel_bins)
        self.act = torch.nn.ReLU()

        # First and Second derivative
        self.first_der = FirstDerivative(self.mel_bins)
        self.second_der = SecondDerative(self.mel_bins)

        # ASPP
        self.aspp = ASPP(in_channels = 1, atrous_rates = [1 , 2,  4,  8, 16], out_channels = 1, kernels = (3,3))

        # CNN
        self.convblock_init = ConvBlock(in_channels = 1, out_channels = 8)
        self.convblock1 = ConvBlock(in_channels = 8, out_channels = 16)
        self.convblock2 = ConvBlock(in_channels = 16, out_channels = 32)
        self.convblock3 = ConvBlock(in_channels = 32, out_channels = 128)
        self.convblock4 = ConvBlock(in_channels = 128, out_channels = 512)
        self.convblock5 = ConvBlock(in_channels = 512, out_channels = self.num_classes * self.num_frames, kernel_conv = 3)

        self.adaptavg2d = nn.AdaptiveAvgPool2d((1))
        self.adaptmax2d = nn.AdaptiveMaxPool2d((1))

        self.final_conv_block = torch.nn.Sequential(
            nn.Conv2d(in_channels = 1, out_channels = 16, kernel_size = 3, padding = 'same'),
            self.act,
            nn.BatchNorm2d(16),
            nn.Conv2d(in_channels = 16, out_channels = 64, kernel_size = 3, padding = 'same'),
            self.act,
            nn.BatchNorm2d(64),
            nn.Conv2d(in_channels = 64, out_channels = 16, kernel_size = 3, padding = 'same'),
            self.act,
            nn.BatchNorm2d(16),
            nn.Conv2d(in_channels = 16, out_channels = 1, kernel_size = 1, padding = 'same'),
            nn.Sigmoid()
        )

        # # Loss function
        self.loss_function = nn.BCELoss()

        # # Init layers
        init_bn(self.spectrogram_norm)
        self.segm_based_stats_val = SEDSegmentBasedMetrics(num_classes = self.num_classes, threshold=0.9)


    def forward(self, x):
        # x [batch_size, sample_rate * length_audio]
        batch_size = x.size()[0]
        #############################
        # Spectrogram extraction
        if self.training:
            with torch.no_grad():
                x = self.waveform_aug(x.unsqueeze(1), sample_rate = self.sample_rate)

        x = x.squeeze(1)
        x = self.spectrogram_extractor(x)
        x = self.logmel_extractor(x)

        # Spectrogram Normalization
        x = x.transpose(1,3)
        x = self.spectrogram_norm(x)
        x = x.transpose(1,3)

        # # Resizing the spectrogram in order to have dimensions that are multiple of 2
        # time_size = int(torch.pow(torch.tensor(2) , torch.floor(torch.log2(torch.tensor(x.shape[2]))) + 1))
        # x = F.interpolate(x, size = (time_size , x.shape[3]))
        # x = F.interpolate(x, scale_factor = (1/4, 1))

        if self.training:
            x = self.spectrogram_augmenter(x)

        with torch.no_grad():
             first_der = self.first_der(x)
             second_der = self.second_der(x)
             x = torch.cat((x, first_der, second_der), dim = 3)


        # ASPP
        x = self.aspp(x)

        # Encoder
        x = self.convblock_init(x, pool_size = (4, 2), pool_type = 'avg+max')
        x = self.convblock1(x, pool_size=(4, 2), pool_type='avg+max')
        x = self.convblock2(x, pool_size=(2, 2), pool_type='avg+max')
        x = self.convblock3(x, pool_size=(2, 2), pool_type='avg+max')
        x = F.dropout2d(x, p=self.drop, training=self.training)
        x = self.convblock4(x, pool_size=(2, 2), pool_type='avg+max')
        x = F.dropout2d(x, p=self.drop, training=self.training)
        x = self.convblock5(x, pool_size=(1, 1), pool_type='avg+max')
        x1 = self.adaptavg2d(x)
        x2 = self.adaptmax2d(x)
        x = x1 + x2
        x = x.reshape(batch_size, 1, self.num_classes, self.num_frames)
        # x = F.dropout(self.act(self.bn_fc_transfer(self.fc_transfer(x))), p = self.drop, training = self.training)
        # x = F.dropout(self.final(x), p = self.drop, training = self.training)
        # x = x.reshape(batch_size, 1, self.num_classes, self.num_frames)
        x = self.final_conv_block(x)
        x = x.squeeze(1)
        return x


    
    def training_step(self, batch, batch_idx):
        audio_features , batch_labels, idx = batch
        out = self(audio_features.squeeze())
        loss = self.loss(out, batch_labels)
        self.log('train/loss', loss)
        return loss

    def validation_step(self, batch, batch_idx):
        audio_features , batch_labels, idx = batch
        out = self(audio_features.squeeze())
        loss = self.loss(out, batch_labels)
        self.log('val/loss', loss, on_epoch = True, prog_bar = True)
        self.segm_based_stats_val.update(out, batch_labels)
        return loss
    
    def validation_epoch_end(self, val_all):
        _, precision_classwise, recall_classwise, fscore_classwise, fscore_general, ER, accuracy, specificity, acc_mir = self.segm_based_stats_val.compute()
        self.log('val/precision_class', torch.mean(precision_classwise))
        self.log('val/recall_class', torch.mean(recall_classwise))
        self.log('val/fscore_class', fscore_classwise)
        self.log('val/fscore_general', fscore_general)
        self.log('val/ER', ER)
        self.log('val/accuracy', accuracy)
        self.log('val/specificity', specificity)
        self.log('val/acc_mir', acc_mir)
        for i in range(recall_classwise.shape[0]):
            self.log("val/recall_{}".format(i+1), recall_classwise[i])
        self.segm_based_stats_val.reset()
        

    def configure_optimizers(self):
        optim =  torch.optim.Adam(self.parameters(), lr = self.lr)
        return {
            "optimizer": optim,
            "lr_scheduler": {
                "scheduler": torch.optim.lr_scheduler.ReduceLROnPlateau(optim, verbose=True, patience = 4),
                "monitor": "val/loss",
                "frequency": 1
            },
     }

    def loss(self, out, label):
        return self.loss_function(out , label)


# Testing
from torchinfo import summary
if __name__ == "__main__":
    config = {
            "max_epochs": 20,
            "batch_size": 6,
            "lr": 0.01,
            "window_size": 512,
            "hop_size": 160,
            "mel_bins": 64,
        }
    test = AuSPP(sample_rate = 16000, window_size = config['window_size'], hop_size = config['hop_size'],
                 mel_bins = config['mel_bins'], num_classes = 10, fmin = 50, fmax = 8000, lr = config['lr'], max_duration = 4, frame_length = 0.05).eval()
    example_spectrogram = torch.randn([2,64000])
    result = test(example_spectrogram)
    print(result.shape)
    print(result)
    summary(test, input_size=(2, 64000))
