from torchmetrics import Metric
import torch
import numpy as np

class SEDSegmentBasedMetrics(Metric):
    def __init__(self, num_classes, threshold = 0.8):
        super().__init__()

        self.threshold = threshold
        self.num_classes = num_classes
        self.add_state("matrix_stats", default = torch.zeros((self.num_classes, 4)), dist_reduce_fx = "sum")
        self.add_state("ER", default = torch.tensor(0.), dist_reduce_fx = "sum")
        self.add_state("total_batches", default = torch.tensor(0), dist_reduce_fx = "sum")

    def update(self, preds, target):
        # removing low probabilites
        if (len(preds.shape) == 2): # In case of single couple of preds/target
            preds = preds.unsqueeze(0)
            target = target.unsqueeze(0)
        thresholded_pred = preds > self.threshold
        thresholded_pred = thresholded_pred.type(torch.float)
        temp_matrix = target-2*thresholded_pred
        # FN = 1 , TN = 0 , TP = -1, FP = -2
        # class-wise (Precision, Recall, F2Score)
        for i in range(self.num_classes):
            self.matrix_stats[i,0] = self.matrix_stats[i,0]  + torch.sum(temp_matrix[:,i,:] == -1) #TP
            self.matrix_stats[i,1] = self.matrix_stats[i,1] + torch.sum(temp_matrix[:,i,:] == 1)  #FN
            self.matrix_stats[i,2] = self.matrix_stats[i,2] + torch.sum(temp_matrix[:,i,:] == -2)  #FP
            self.matrix_stats[i,3] = self.matrix_stats[i,3] + torch.sum(temp_matrix[:,i,:] == 0)  #TN
        # segment-wise (ER)
        sub = torch.tensor(0, dtype = torch.float, device = preds.device)
        dele = torch.tensor(0, dtype = torch.float, device =  preds.device)
        ins = torch.tensor(0, dtype = torch.float, device =  preds.device)
        n = torch.tensor(0, dtype = torch.float, device =  preds.device)
        for i in range(temp_matrix.shape[2]):
            sub = sub + torch.minimum(torch.sum(temp_matrix[:,:,i] == 1), torch.sum(temp_matrix[:,:, i] == -2))
            dele = dele + torch.maximum(torch.tensor(0, device = preds.device), torch.sum(temp_matrix[:,:,i] == 1)  - torch.sum(temp_matrix[:,:,i] == -2))
            ins = ins + torch.maximum(torch.tensor(0, device = preds.device), torch.sum(temp_matrix[:,:,i] == -2) - torch.sum(temp_matrix[:,:,i] == 1))
            n = n + torch.sum(target[:,:,i])
        er = (sub + dele + ins)/n
        self.ER = self.ER + torch.sum(er)
        self.total_batches = self.total_batches + 1

    def compute(self):
        # Here we can return a dict of all the metrics with the matrix statistics
        precision_classwise = torch.div(self.matrix_stats[:,0], self.matrix_stats[:,0] + self.matrix_stats[:,2])
        recall_classwise = torch.div(self.matrix_stats[:,0], self.matrix_stats[:,0] + self.matrix_stats[:,1])
        fscore_classwise = torch.div(2 * precision_classwise * recall_classwise, precision_classwise + recall_classwise)
        fscore_general = torch.sum(self.matrix_stats, dim=0)
        fscore_general = torch.div(2 * fscore_general[0], 2 * fscore_general[0] + fscore_general[1] + fscore_general[2] )
        self.ER = torch.div(self.ER, self.total_batches)
        accuracy = torch.div(self.matrix_stats[:,0] + self.matrix_stats[:,3], self.matrix_stats[:,0] + self.matrix_stats[:,3] + self.matrix_stats[:, 2] + self.matrix_stats[:, 1]  )
        specificity = torch.div(self.matrix_stats[:,3], self.matrix_stats[:,3] + self.matrix_stats[:,2])
        acc_mir = torch.div(self.matrix_stats[:,0] , self.matrix_stats[:,0] + self.matrix_stats[:, 2] + self.matrix_stats[:, 1])
        return self.matrix_stats, torch.nan_to_num(precision_classwise, nan = 0.0), torch.nan_to_num(recall_classwise, nan = 0.0), torch.nan_to_num(fscore_classwise,nan = 0.0), fscore_general, \
                 self.ER, accuracy, specificity, acc_mir

if __name__ == '__main__':
    test = SEDSegmentBasedMetrics(num_classes = 5)
    gt = torch.tensor(np.array([[1, 1 ,1], [0 ,0 ,1], [1, 0, 0], [1, 1, 1], [0, 1, 1]]))
    gt = torch.stack((gt, gt, gt, gt))
    pred = torch.tensor(np.array([[0, 0 ,0], [0 ,0 ,0], [0, 0, 0], [0, 0, 0], [0, 0, 0]]))
    pred = torch.stack((pred, pred, pred, pred))
    test.update(pred, gt)
    test.update(pred, gt)
    print(test.compute())