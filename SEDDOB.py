import torch
from torch.utils.data import DataLoader, Dataset
from pytorch_lightning import LightningDataModule
import librosa as lb
import os
import matplotlib.pyplot as plt
import numpy as np
import torchlibrosa as tl
import librosa.display

map_labels = ['breaking_glass', 'car_horn', 'gunshot', 'siren', 'slap', 'scream','cry','jackhammer','car_alarm','smoke_alarm']

def mapping_id_to_class(id_class):
    return map_labels[id_class]

def mapping_class_to_id(string_class):
    index = map_labels.index(string_class)
    return index



class SyntDataset(Dataset):

    def __init__(self, file_path, folderList, audio_samples = 1000, frame_length = 0.05, num_classes = 4):
        #initialize lists to hold file names, labels, and folder numbers
        self.file_names = []
        self.ids = []
        self.max_duration = 4 # seconds
        self.fs = 16000 # max freq 
        self.audio_samples = audio_samples
        self.frame_length = frame_length
        self.num_classes = num_classes
        self.frames = int(self.max_duration/self.frame_length)
        for i in range(self.audio_samples):
            if i // (self.audio_samples //10) + 1 in folderList: ## Selected Folds
                self.file_names.append(i)
        self.file_path = file_path
        self.folderList = folderList





    def __getitem__(self, index):
        # format the file path and load the file
        file_selected = self.file_names[index]
        path = os.path.join(self.file_path, "syntAudio", "fold{}".format( file_selected // (self.audio_samples //10) + 1), "{}.wav".format(file_selected))

        sound, _ = lb.load(path, mono = True, sr = self.fs, res_type = 'polyphase')
        sound = sound.reshape(-1,1)
        
        soundData = torch.tensor(sound.reshape(-1), dtype=torch.float).unsqueeze(0)
        matrix_gt = np.zeros((self.num_classes, self.frames))

        with open(os.path.join(self.file_path, "labels", "fold{}".format(file_selected // (self.audio_samples //10) + 1),  "{}.txt".format(file_selected)), "r") as file_in:
            for line in file_in:
                if line != "\n":
                    line = line.split('\t')
                    # read onset
                    onset = np.float32(line[0])
                    onset_frame = int(np.floor(onset/self.frame_length))
                    offset = np.float32(line[1])
                    offset_frame = int(np.ceil(offset/self.frame_length))
                    class_id = line[2][:-1]
                    matrix_gt[mapping_class_to_id(class_id), onset_frame:offset_frame] = 1
        matrix_gt = torch.tensor(matrix_gt, dtype=torch.float)
        return soundData, matrix_gt, index
    
    def __len__(self):
        return len(self.file_names)

    
class SyntDatasetModule(LightningDataModule):
    
    def __init__(self, file_path, folderListTraining, folderListValidation, batch_size, audio_samples = 1000, frame_length = 0.05, num_classes = 4, transform = None):
        # Init 
        super().__init__()
        self.file_path = file_path
        self.folderListTraining = folderListTraining 
        self.folderListValidation = folderListValidation
        self.batch_size = batch_size
        self.audio_samples = audio_samples
        self.frame_length = frame_length
        self.num_classes = num_classes
        self.transform = transform

    def prepare_data(self) -> None:
        '''
            Nothing to do
        '''
        pass

    def setup(self, stage = None) -> None:
        '''
            Nothing to do
        '''
        pass

    def train_dataloader(self):
        return DataLoader(SyntDataset(self.file_path, self.folderListTraining, self.audio_samples, self.frame_length, self.num_classes), num_workers = 0, batch_size = self.batch_size, shuffle = True,  drop_last = False)
    
    def val_dataloader(self):
        return DataLoader(SyntDataset(self.file_path, self.folderListValidation, self.audio_samples, self.frame_length, self.num_classes), num_workers = 0, batch_size = self.batch_size, shuffle = False, drop_last = False)

    def test_dataloader(self):
        return DataLoader(SyntDataset(self.file_path, self.folderListValidation, self.audio_samples, self.frame_length, self.num_classes), num_workers = 0, batch_size = self.batch_size, shuffle = False, drop_last = False)


    #### TEST DataLoader ####
if __name__ == '__main__':
    file_path = 'SimulatedBusDataset'

    train_set = SyntDataset(file_path, [1, 2, 3, 4, 5, 6, 7, 8, 10])
    test_set = SyntDataset(file_path, [9])
    test, label, path = train_set[5]
    print(test.shape)
    print(label.shape)
    print("Train set size: " + str(len(train_set)))
    print("Test set size: " + str(len(test_set)))

    #### TEST DataModule ####

    datamodule_sed = SyntDatasetModule(file_path, [1, 2, 3, 4, 5, 6, 7, 8, 10], [9], batch_size = 16)
    dataloader_datamodule_training = datamodule_sed.train_dataloader()
    dataloader_datamodule_val = datamodule_sed.val_dataloader()
    print("Train dataloader size: " + str(len(dataloader_datamodule_training)))
    print("Val/Test dataloader size: " + str(len(dataloader_datamodule_val)))
    test1, label1, path1 = next(iter(dataloader_datamodule_training))
    print(test1.shape)
    print(label1.shape)

    # Test torchlibrosa in order to perform Spectrogram and augmentations
    test1 = test1.squeeze()
    win_length = 1024
    hop_length = 380
    sr = 16000
    # n_mels = 64
    spectrogram_extractor = tl.Spectrogram(n_fft = win_length, win_length = win_length, hop_length = hop_length)
    sp = spectrogram_extractor.forward(test1)   # (batch_size, 1, time_steps, freq_bins)

    logmel_extractor = tl.LogmelFilterBank(sr = sr, n_fft = win_length)
    logmel = logmel_extractor.forward(sp)   # (batch_size, 1, time_steps, mel_bins)

    plt.figure(1)
    librosa.display.waveshow(test1[0,:].numpy(), sr = sr)
    plt.title('Audio waveform: {}.txt'.format(path1[0]))
    plt.figure(2)
    plt.imshow(label1[0,:,:].numpy(), cmap = 'jet')
    plt.title('GT {}'.format(path1[0]))
    plt.figure(3)
    plt.imshow(np.transpose(logmel[0,0,:,:].numpy()))
    plt.title('LogMel-Spectrogram file: {}.txt'.format(path1[0]))
    plt.show()