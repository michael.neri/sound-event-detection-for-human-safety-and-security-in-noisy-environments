import torch
from pytorch_lightning import LightningModule
from SEDSegmentBasedMetrics import SEDSegmentBasedMetrics
from pretrain.models import Cnn14
import torch.nn.functional as F
from torchlibrosa import SpecAugmentation, Spectrogram, LogmelFilterBank
import torchvision.models

## Initialization of layers
def init_layer(layer):
    """Initialize a Linear or Convolutional layer. """
    torch.nn.init.xavier_uniform_(layer.weight)
 
    if hasattr(layer, 'bias'):
        if layer.bias is not None:
            layer.bias.data.fill_(0.)
               
def init_bn(bn):
    """Initialize a Batchnorm layer. """
    bn.bias.data.fill_(0.)
    bn.weight.data.fill_(1.)

## INTERNAL MODULES
class ConvBlock(torch.nn.Module):
    def __init__(self, in_channels, out_channels, last = False):
        super(ConvBlock, self).__init__()
        self.last = last
        self.conv1 = torch.nn.Conv2d(in_channels=in_channels, 
                              out_channels=out_channels,
                              kernel_size=(3, 3), stride=(1, 1),
                              padding=(1, 1), bias=False)
                              
        self.conv2 = torch.nn.Conv2d(in_channels=out_channels, 
                              out_channels=out_channels,
                              kernel_size=(3, 3), stride=(1, 1),
                              padding=(1, 1), bias=False)
                              
        self.bn1 = torch.nn.BatchNorm2d(out_channels)
        self.bn2 = torch.nn.BatchNorm2d(out_channels)
        self.init_weight()
        
    def init_weight(self):
        init_layer(self.conv1)
        init_layer(self.conv2)
        init_bn(self.bn1)
        init_bn(self.bn2)

        
    def forward(self, input, pool_size=(2, 2), pool_type='avg'):
        
        x = input
        x = F.relu_(self.bn1(self.conv1(x)))
        if not self.last:
            x = F.relu_(self.bn2(self.conv2(x)))
        else:
            x = self.bn2(self.conv2(x))
        if pool_type == 'max':
            x = F.max_pool2d(x, kernel_size=pool_size)
        elif pool_type == 'avg':
            x = F.avg_pool2d(x, kernel_size=pool_size)
        elif pool_type == 'avg+max':
            x1 = F.avg_pool2d(x, kernel_size=pool_size)
            x2 = F.max_pool2d(x, kernel_size=pool_size)
            x = x1 + x2
        else:
            raise Exception('Incorrect argument!')
        
        return x

class ConvPreWavBlock(torch.nn.Module):
    def __init__(self, in_channels, out_channels):
        
        super(ConvPreWavBlock, self).__init__()
        
        self.conv1 = torch.nn.Conv1d(in_channels=in_channels, 
                              out_channels=out_channels,
                              kernel_size=3, stride=1,
                              padding=1, bias=False)
                              
        self.conv2 = torch.nn.Conv1d(in_channels=out_channels, 
                              out_channels=out_channels,
                              kernel_size=3, stride=1, dilation=2, 
                              padding=2, bias=False)
                              
        self.bn1 = torch.nn.BatchNorm1d(out_channels)
        self.bn2 = torch.nn.BatchNorm1d(out_channels)

        self.init_weight()
        
    def init_weight(self):
        init_layer(self.conv1)
        init_layer(self.conv2)
        init_bn(self.bn1)
        init_bn(self.bn2)

        
    def forward(self, input, pool_size):
        x = input
        x = F.relu_(self.bn1(self.conv1(x)))
        x = F.relu_(self.bn2(self.conv2(x)))
        x = F.max_pool1d(x, kernel_size=pool_size)
        return x

###################### GENERAL-PURPOSE MODELS
class VGG16_sed(LightningModule):
    def __init__(self, sample_rate, window_size, hop_size, mel_bins, fmin, 
        fmax, lr = 0.001, max_duration = 4, num_classes = 4, frame_length = 0.05):
        super(VGG16_sed, self).__init__()
        # Changing head of VGG
        self.base = torchvision.models.vgg16_bn()
        self.base.classifier[6] = torch.nn.Linear(4096, 2048)

        self.num_classes = num_classes
        self.frame_length = frame_length
        self.max_duration = max_duration
        self.lr = lr

        window = 'hann'
        center = True
        pad_mode = 'reflect'
        ref = 1.0
        amin = 1e-10
        top_db = None

        # Spectrogram extractor
        self.spectrogram_extractor = Spectrogram(n_fft=window_size, hop_length=hop_size, 
            win_length=window_size, window=window, center=center, pad_mode=pad_mode, 
            freeze_parameters=True)

        # Logmel feature extractor
        self.logmel_extractor = LogmelFilterBank(sr=sample_rate, n_fft=window_size, 
            n_mels=mel_bins, fmin=fmin, fmax=fmax, ref=ref, amin=amin, top_db=top_db, 
            freeze_parameters=True)

        # Spec augmenter
        self.spec_augmenter = SpecAugmentation(time_drop_width=64, time_stripes_num=2, 
            freq_drop_width=8, freq_stripes_num=2)

        self.bn0 = torch.nn.BatchNorm2d(64)

        # activaction function
        self.act = torch.nn.ReLU(inplace=False)
        self.loss_function = torch.nn.BCELoss()
        self.num_frames = int(self.max_duration/self.frame_length)
        # Transfer to another task layer
        self.fc_transfer = torch.nn.Linear(2048, 1024, bias=False)
        self.bn_fc_transfer = torch.nn.BatchNorm1d(1024)
        self.final = torch.nn.Linear(1024, self.num_classes * self.num_frames)
        # Compute Segment-Based Statistics
        self.segm_based_stats_val = SEDSegmentBasedMetrics(num_classes = self.num_classes, threshold=0.9)
        self.init_weights()

    def init_weights(self):
        init_layer(self.fc_transfer)
        init_layer(self.final)
        init_bn(self.bn_fc_transfer)

    def forward(self, input):
        """Input: (batch_size, data_length)"""
        batch_size = input.shape[0]
        # Log mel spectrogram
        x = self.spectrogram_extractor(input)   # (batch_size, 1, time_steps, freq_bins)
        x = self.logmel_extractor(x)    # (batch_size, 1, time_steps, mel_bins)
        
        x = x.transpose(1, 3)
        x = self.bn0(x)
        x = x.transpose(1, 3)

        if self.training:
            x = self.spec_augmenter(x)
        # vgg expects rgb images -> repeat the spectrogram three times
        x = torch.cat((x, x, x), dim = 1)
        embedding = self.base(x)
        embedding = F.dropout(self.act(self.bn_fc_transfer(self.fc_transfer(embedding))), p = 0.2, training = self.training)
        embedding = F.dropout(self.final(embedding), p = 0.2, training = self.training)
        embedding = torch.sigmoid(embedding.reshape((batch_size, 1, self.num_classes, self.num_frames)))
        embedding = embedding.squeeze(1)
        return embedding

    def training_step(self, batch, batch_idx):
        audio_features , batch_labels, idx = batch
        out = self(audio_features.squeeze())
        loss = self.loss(out, batch_labels)
        self.log('train/loss', loss)
        return loss

    def validation_step(self, batch, batch_idx):
        audio_features , batch_labels, idx = batch
        out = self(audio_features.squeeze())
        loss = self.loss(out, batch_labels)
        self.log('val/loss', loss, on_epoch = True, prog_bar = True)
        self.segm_based_stats_val.update(out, batch_labels)
        return loss
    
    def validation_epoch_end(self, val_all):
        _, precision_classwise, recall_classwise, fscore_classwise, fscore_general, ER, accuracy, specificity, acc_mir = self.segm_based_stats_val.compute()
        self.log('val/precision_class', torch.mean(precision_classwise))
        self.log('val/recall_class', torch.mean(recall_classwise))
        self.log('val/fscore_class', fscore_classwise)
        self.log('val/fscore_general', fscore_general)
        self.log('val/ER', ER)
        self.log('val/accuracy', accuracy)
        self.log('val/specificity', specificity)
        self.log('val/acc_mir', acc_mir)
        for i in range(recall_classwise.shape[0]):
            self.log("val/recall_{}".format(i+1), recall_classwise[i])
        self.segm_based_stats_val.reset()
        

    def configure_optimizers(self):
        optim =  torch.optim.AdamW(self.parameters(), lr = self.lr)
        return {
            "optimizer": optim,
            "lr_scheduler": {
                "scheduler": torch.optim.lr_scheduler.ReduceLROnPlateau(optim, verbose=True, patience = 4),
                "monitor": "val/loss",
                "frequency": 1
            },
     }

    def loss(self, out, label):
        return self.loss_function(out , label)

class VGG19_sed(LightningModule):
    def __init__(self, sample_rate, window_size, hop_size, mel_bins, fmin, 
        fmax, lr = 0.001, max_duration = 4, num_classes = 4, frame_length = 0.05):
        super(VGG19_sed, self).__init__()
        # Changing head of VGG
        self.base = torchvision.models.vgg19_bn()
        self.base.classifier[6] = torch.nn.Linear(4096, 2048)

        self.num_classes = num_classes
        self.frame_length = frame_length
        self.max_duration = max_duration
        self.lr = lr

        window = 'hann'
        center = True
        pad_mode = 'reflect'
        ref = 1.0
        amin = 1e-10
        top_db = None

        # Spectrogram extractor
        self.spectrogram_extractor = Spectrogram(n_fft=window_size, hop_length=hop_size, 
            win_length=window_size, window=window, center=center, pad_mode=pad_mode, 
            freeze_parameters=True)

        # Logmel feature extractor
        self.logmel_extractor = LogmelFilterBank(sr=sample_rate, n_fft=window_size, 
            n_mels=mel_bins, fmin=fmin, fmax=fmax, ref=ref, amin=amin, top_db=top_db, 
            freeze_parameters=True)

        # Spec augmenter
        self.spec_augmenter = SpecAugmentation(time_drop_width=64, time_stripes_num=2, 
            freq_drop_width=8, freq_stripes_num=2)

        self.bn0 = torch.nn.BatchNorm2d(64)

        # activaction function
        self.act = torch.nn.ReLU(inplace=False)
        self.loss_function = torch.nn.BCELoss()
        self.num_frames = int(self.max_duration/self.frame_length)
        # Transfer to another task layer
        self.fc_transfer = torch.nn.Linear(2048, 1024, bias=False)
        self.bn_fc_transfer = torch.nn.BatchNorm1d(1024)
        self.final = torch.nn.Linear(1024, self.num_classes * self.num_frames)
        # Compute Segment-Based Statistics
        self.segm_based_stats_val = SEDSegmentBasedMetrics(num_classes = self.num_classes, threshold=0.9)
        self.init_weights()

    def init_weights(self):
        init_layer(self.fc_transfer)
        init_layer(self.final)
        init_bn(self.bn_fc_transfer)

    def forward(self, input):
        """Input: (batch_size, data_length)"""
        batch_size = input.shape[0]
        # Log mel spectrogram
        x = self.spectrogram_extractor(input)   # (batch_size, 1, time_steps, freq_bins)
        x = self.logmel_extractor(x)    # (batch_size, 1, time_steps, mel_bins)
        
        x = x.transpose(1, 3)
        x = self.bn0(x)
        x = x.transpose(1, 3)

        if self.training:
            x = self.spec_augmenter(x)
        # vgg expects rgb images -> repeat the spectrogram three times
        x = torch.cat((x, x, x), dim = 1)
        embedding = self.base(x)
        embedding = F.dropout(self.act(self.bn_fc_transfer(self.fc_transfer(embedding))), p = 0.2, training = self.training)
        embedding = F.dropout(self.final(embedding), p = 0.2, training = self.training)
        embedding = torch.sigmoid(embedding.reshape((batch_size, 1, self.num_classes, self.num_frames)))
        embedding = embedding.squeeze(1)
        return embedding

    def training_step(self, batch, batch_idx):
        audio_features , batch_labels, idx = batch
        out = self(audio_features.squeeze())
        loss = self.loss(out, batch_labels)
        self.log('train/loss', loss)
        return loss

    def validation_step(self, batch, batch_idx):
        audio_features , batch_labels, idx = batch
        out = self(audio_features.squeeze())
        loss = self.loss(out, batch_labels)
        self.log('val/loss', loss, on_epoch = True, prog_bar = True)
        self.segm_based_stats_val.update(out, batch_labels)
        return loss
    
    def validation_epoch_end(self, val_all):
        _, precision_classwise, recall_classwise, fscore_classwise, fscore_general, ER, accuracy, specificity, acc_mir = self.segm_based_stats_val.compute()
        self.log('val/precision_class', torch.mean(precision_classwise))
        self.log('val/recall_class', torch.mean(recall_classwise))
        self.log('val/fscore_class', fscore_classwise)
        self.log('val/fscore_general', fscore_general)
        self.log('val/ER', ER)
        self.log('val/accuracy', accuracy)
        self.log('val/specificity', specificity)
        self.log('val/acc_mir', acc_mir)
        for i in range(recall_classwise.shape[0]):
            self.log("val/recall_{}".format(i+1), recall_classwise[i])
        self.segm_based_stats_val.reset()
        

    def configure_optimizers(self):
        optim =  torch.optim.AdamW(self.parameters(), lr = self.lr)
        return {
            "optimizer": optim,
            "lr_scheduler": {
                "scheduler": torch.optim.lr_scheduler.ReduceLROnPlateau(optim, verbose=True, patience = 4),
                "monitor": "val/loss",
                "frequency": 1
            },
     }

    def loss(self, out, label):
        return self.loss_function(out , label)        

class ResNet18_sed(LightningModule):
    def __init__(self, sample_rate, window_size, hop_size, mel_bins, fmin, 
        fmax, lr = 0.001, max_duration = 4, num_classes = 4, frame_length = 0.05):
        super(ResNet18_sed, self).__init__()
        # Changing head of VGG
        self.base = torchvision.models.resnet18()
        self.base.fc = torch.nn.Linear(512, 2048)

        self.num_classes = num_classes
        self.frame_length = frame_length
        self.max_duration = max_duration
        self.lr = lr

        window = 'hann'
        center = True
        pad_mode = 'reflect'
        ref = 1.0
        amin = 1e-10
        top_db = None

        # Spectrogram extractor
        self.spectrogram_extractor = Spectrogram(n_fft=window_size, hop_length=hop_size, 
            win_length=window_size, window=window, center=center, pad_mode=pad_mode, 
            freeze_parameters=True)

        # Logmel feature extractor
        self.logmel_extractor = LogmelFilterBank(sr=sample_rate, n_fft=window_size, 
            n_mels=mel_bins, fmin=fmin, fmax=fmax, ref=ref, amin=amin, top_db=top_db, 
            freeze_parameters=True)

        # Spec augmenter
        self.spec_augmenter = SpecAugmentation(time_drop_width=64, time_stripes_num=2, 
            freq_drop_width=8, freq_stripes_num=2)

        self.bn0 = torch.nn.BatchNorm2d(64)

        # activaction function
        self.act = torch.nn.ReLU(inplace=False)
        self.loss_function = torch.nn.BCELoss()
        self.num_frames = int(self.max_duration/self.frame_length)
        # Transfer to another task layer
        self.fc_transfer = torch.nn.Linear(2048, 1024, bias=False)
        self.bn_fc_transfer = torch.nn.BatchNorm1d(1024)
        self.final = torch.nn.Linear(1024, self.num_classes * self.num_frames)
        # Compute Segment-Based Statistics
        self.segm_based_stats_val = SEDSegmentBasedMetrics(num_classes = self.num_classes, threshold=0.9)
        self.init_weights()

    def init_weights(self):
        init_layer(self.fc_transfer)
        init_layer(self.final)
        init_bn(self.bn_fc_transfer)

    def forward(self, input):
        """Input: (batch_size, data_length)"""
        batch_size = input.shape[0]
        # Log mel spectrogram
        x = self.spectrogram_extractor(input)   # (batch_size, 1, time_steps, freq_bins)
        x = self.logmel_extractor(x)    # (batch_size, 1, time_steps, mel_bins)
        
        x = x.transpose(1, 3)
        x = self.bn0(x)
        x = x.transpose(1, 3)

        if self.training:
            x = self.spec_augmenter(x)
        # vgg expects rgb images -> repeat the spectrogram three times
        x = torch.cat((x, x, x), dim = 1)
        embedding = self.base(x)
        embedding = F.dropout(self.act(self.bn_fc_transfer(self.fc_transfer(embedding))), p = 0.2, training = self.training)
        embedding = F.dropout(self.final(embedding), p = 0.2, training = self.training)
        embedding = torch.sigmoid(embedding.reshape((batch_size, 1, self.num_classes, self.num_frames)))
        embedding = embedding.squeeze(1)
        return embedding

    def training_step(self, batch, batch_idx):
        audio_features , batch_labels, idx = batch
        out = self(audio_features.squeeze())
        loss = self.loss(out, batch_labels)
        self.log('train/loss', loss)
        return loss

    def validation_step(self, batch, batch_idx):
        audio_features , batch_labels, idx = batch
        out = self(audio_features.squeeze())
        loss = self.loss(out, batch_labels)
        self.log('val/loss', loss, on_epoch = True, prog_bar = True)
        self.segm_based_stats_val.update(out, batch_labels)
        return loss
    
    def validation_epoch_end(self, val_all):
        _, precision_classwise, recall_classwise, fscore_classwise, fscore_general, ER, accuracy, specificity, acc_mir = self.segm_based_stats_val.compute()
        self.log('val/precision_class', torch.mean(precision_classwise))
        self.log('val/recall_class', torch.mean(recall_classwise))
        self.log('val/fscore_class', fscore_classwise)
        self.log('val/fscore_general', fscore_general)
        self.log('val/ER', ER)
        self.log('val/accuracy', accuracy)
        self.log('val/specificity', specificity)
        self.log('val/acc_mir', acc_mir)
        for i in range(recall_classwise.shape[0]):
            self.log("val/recall_{}".format(i+1), recall_classwise[i])
        self.segm_based_stats_val.reset()
        

    def configure_optimizers(self):
        optim =  torch.optim.AdamW(self.parameters(), lr = self.lr)
        return {
            "optimizer": optim,
            "lr_scheduler": {
                "scheduler": torch.optim.lr_scheduler.ReduceLROnPlateau(optim, verbose=True, patience = 4),
                "monitor": "val/loss",
                "frequency": 1
            },
     }

    def loss(self, out, label):
        return self.loss_function(out , label)            

class ResNet34_sed(LightningModule):
    def __init__(self, sample_rate, window_size, hop_size, mel_bins, fmin, 
        fmax, lr = 0.001, max_duration = 4, num_classes = 4, frame_length = 0.05):
        super(ResNet34_sed, self).__init__()
        # Changing head of VGG
        self.base = torchvision.models.resnet34()
        self.base.fc = torch.nn.Linear(512, 2048)

        self.num_classes = num_classes
        self.frame_length = frame_length
        self.max_duration = max_duration
        self.lr = lr

        window = 'hann'
        center = True
        pad_mode = 'reflect'
        ref = 1.0
        amin = 1e-10
        top_db = None

        # Spectrogram extractor
        self.spectrogram_extractor = Spectrogram(n_fft=window_size, hop_length=hop_size, 
            win_length=window_size, window=window, center=center, pad_mode=pad_mode, 
            freeze_parameters=True)

        # Logmel feature extractor
        self.logmel_extractor = LogmelFilterBank(sr=sample_rate, n_fft=window_size, 
            n_mels=mel_bins, fmin=fmin, fmax=fmax, ref=ref, amin=amin, top_db=top_db, 
            freeze_parameters=True)

        # Spec augmenter
        self.spec_augmenter = SpecAugmentation(time_drop_width=64, time_stripes_num=2, 
            freq_drop_width=8, freq_stripes_num=2)

        self.bn0 = torch.nn.BatchNorm2d(64)

        # activaction function
        self.act = torch.nn.ReLU(inplace=False)
        self.loss_function = torch.nn.BCELoss()
        self.num_frames = int(self.max_duration/self.frame_length)
        # Transfer to another task layer
        self.fc_transfer = torch.nn.Linear(2048, 1024, bias=False)
        self.bn_fc_transfer = torch.nn.BatchNorm1d(1024)
        self.final = torch.nn.Linear(1024, self.num_classes * self.num_frames)
        # Compute Segment-Based Statistics
        self.segm_based_stats_val = SEDSegmentBasedMetrics(num_classes = self.num_classes, threshold=0.9)
        self.init_weights()

    def init_weights(self):
        init_layer(self.fc_transfer)
        init_layer(self.final)
        init_bn(self.bn_fc_transfer)

    def forward(self, input):
        """Input: (batch_size, data_length)"""
        batch_size = input.shape[0]
        # Log mel spectrogram
        x = self.spectrogram_extractor(input)   # (batch_size, 1, time_steps, freq_bins)
        x = self.logmel_extractor(x)    # (batch_size, 1, time_steps, mel_bins)
        
        x = x.transpose(1, 3)
        x = self.bn0(x)
        x = x.transpose(1, 3)

        if self.training:
            x = self.spec_augmenter(x)
        # vgg expects rgb images -> repeat the spectrogram three times
        x = torch.cat((x, x, x), dim = 1)
        embedding = self.base(x)
        embedding = F.dropout(self.act(self.bn_fc_transfer(self.fc_transfer(embedding))), p = 0.2, training = self.training)
        embedding = F.dropout(self.final(embedding), p = 0.2, training = self.training)
        embedding = torch.sigmoid(embedding.reshape((batch_size, 1, self.num_classes, self.num_frames)))
        embedding = embedding.squeeze(1)
        return embedding

    def training_step(self, batch, batch_idx):
        audio_features , batch_labels, idx = batch
        out = self(audio_features.squeeze())
        loss = self.loss(out, batch_labels)
        self.log('train/loss', loss)
        return loss

    def validation_step(self, batch, batch_idx):
        audio_features , batch_labels, idx = batch
        out = self(audio_features.squeeze())
        loss = self.loss(out, batch_labels)
        self.log('val/loss', loss, on_epoch = True, prog_bar = True)
        self.segm_based_stats_val.update(out, batch_labels)
        return loss
    
    def validation_epoch_end(self, val_all):
        _, precision_classwise, recall_classwise, fscore_classwise, fscore_general, ER, accuracy, specificity, acc_mir = self.segm_based_stats_val.compute()
        self.log('val/precision_class', torch.mean(precision_classwise))
        self.log('val/recall_class', torch.mean(recall_classwise))
        self.log('val/fscore_class', fscore_classwise)
        self.log('val/fscore_general', fscore_general)
        self.log('val/ER', ER)
        self.log('val/accuracy', accuracy)
        self.log('val/specificity', specificity)
        self.log('val/acc_mir', acc_mir)
        for i in range(recall_classwise.shape[0]):
            self.log("val/recall_{}".format(i+1), recall_classwise[i])
        self.segm_based_stats_val.reset()
        

    def configure_optimizers(self):
        optim =  torch.optim.AdamW(self.parameters(), lr = self.lr)
        return {
            "optimizer": optim,
            "lr_scheduler": {
                "scheduler": torch.optim.lr_scheduler.ReduceLROnPlateau(optim, verbose=True, patience = 4),
                "monitor": "val/loss",
                "frequency": 1
            },
     }

    def loss(self, out, label):
        return self.loss_function(out , label)

class ResNet50_sed(LightningModule):
    def __init__(self, sample_rate, window_size, hop_size, mel_bins, fmin, 
        fmax, lr = 0.001, max_duration = 4, num_classes = 4, frame_length = 0.05):
        super(ResNet50_sed, self).__init__()
        # Changing head of VGG
        self.base = torchvision.models.resnet50()
        self.base.fc = torch.nn.Identity()

        self.num_classes = num_classes
        self.frame_length = frame_length
        self.max_duration = max_duration
        self.lr = lr

        window = 'hann'
        center = True
        pad_mode = 'reflect'
        ref = 1.0
        amin = 1e-10
        top_db = None

        # Spectrogram extractor
        self.spectrogram_extractor = Spectrogram(n_fft=window_size, hop_length=hop_size, 
            win_length=window_size, window=window, center=center, pad_mode=pad_mode, 
            freeze_parameters=True)

        # Logmel feature extractor
        self.logmel_extractor = LogmelFilterBank(sr=sample_rate, n_fft=window_size, 
            n_mels=mel_bins, fmin=fmin, fmax=fmax, ref=ref, amin=amin, top_db=top_db, 
            freeze_parameters=True)

        # Spec augmenter
        self.spec_augmenter = SpecAugmentation(time_drop_width=64, time_stripes_num=2, 
            freq_drop_width=8, freq_stripes_num=2)

        self.bn0 = torch.nn.BatchNorm2d(64)

        # activaction function
        self.act = torch.nn.ReLU(inplace=False)
        self.loss_function = torch.nn.BCELoss()
        self.num_frames = int(self.max_duration/self.frame_length)
        # Transfer to another task layer
        self.fc_transfer = torch.nn.Linear(2048, 1024, bias=False)
        self.bn_fc_transfer = torch.nn.BatchNorm1d(1024)
        self.final = torch.nn.Linear(1024, self.num_classes * self.num_frames)
        # Compute Segment-Based Statistics
        self.segm_based_stats_val = SEDSegmentBasedMetrics(num_classes = self.num_classes, threshold=0.9)
        self.init_weights()

    def init_weights(self):
        init_layer(self.fc_transfer)
        init_layer(self.final)
        init_bn(self.bn_fc_transfer)

    def forward(self, input):
        """Input: (batch_size, data_length)"""
        batch_size = input.shape[0]
        # Log mel spectrogram
        x = self.spectrogram_extractor(input)   # (batch_size, 1, time_steps, freq_bins)
        x = self.logmel_extractor(x)    # (batch_size, 1, time_steps, mel_bins)
        
        x = x.transpose(1, 3)
        x = self.bn0(x)
        x = x.transpose(1, 3)

        if self.training:
            x = self.spec_augmenter(x)
        # vgg expects rgb images -> repeat the spectrogram three times
        x = torch.cat((x, x, x), dim = 1)
        embedding = self.base(x)
        embedding = F.dropout(self.act(self.bn_fc_transfer(self.fc_transfer(embedding))), p = 0.2, training = self.training)
        embedding = F.dropout(self.final(embedding), p = 0.2, training = self.training)
        embedding = torch.sigmoid(embedding.reshape((batch_size, 1, self.num_classes, self.num_frames)))
        embedding = embedding.squeeze(1)
        return embedding

    def training_step(self, batch, batch_idx):
        audio_features , batch_labels, idx = batch
        out = self(audio_features.squeeze())
        loss = self.loss(out, batch_labels)
        self.log('train/loss', loss)
        return loss

    def validation_step(self, batch, batch_idx):
        audio_features , batch_labels, idx = batch
        out = self(audio_features.squeeze())
        loss = self.loss(out, batch_labels)
        self.log('val/loss', loss, on_epoch = True, prog_bar = True)
        self.segm_based_stats_val.update(out, batch_labels)
        return loss
    
    def validation_epoch_end(self, val_all):
        _, precision_classwise, recall_classwise, fscore_classwise, fscore_general, ER, accuracy, specificity, acc_mir = self.segm_based_stats_val.compute()
        self.log('val/precision_class', torch.mean(precision_classwise))
        self.log('val/recall_class', torch.mean(recall_classwise))
        self.log('val/fscore_class', fscore_classwise)
        self.log('val/fscore_general', fscore_general)
        self.log('val/ER', ER)
        self.log('val/accuracy', accuracy)
        self.log('val/specificity', specificity)
        self.log('val/acc_mir', acc_mir)
        for i in range(recall_classwise.shape[0]):
            self.log("val/recall_{}".format(i+1), recall_classwise[i])
        self.segm_based_stats_val.reset()
        

    def configure_optimizers(self):
        optim =  torch.optim.AdamW(self.parameters(), lr = self.lr)
        return {
            "optimizer": optim,
            "lr_scheduler": {
                "scheduler": torch.optim.lr_scheduler.ReduceLROnPlateau(optim, verbose=True, patience = 4),
                "monitor": "val/loss",
                "frequency": 1
            },
     }

    def loss(self, out, label):
        return self.loss_function(out , label)

class ResNet101_sed(LightningModule):
    def __init__(self, sample_rate, window_size, hop_size, mel_bins, fmin, 
        fmax, lr = 0.001, max_duration = 4, num_classes = 4, frame_length = 0.05):
        super(ResNet101_sed, self).__init__()
        # Changing head of VGG
        self.base = torchvision.models.resnet101()
        self.base.fc = torch.nn.Identity()
        self.num_classes = num_classes
        self.frame_length = frame_length
        self.max_duration = max_duration
        self.lr = lr

        window = 'hann'
        center = True
        pad_mode = 'reflect'
        ref = 1.0
        amin = 1e-10
        top_db = None

        # Spectrogram extractor
        self.spectrogram_extractor = Spectrogram(n_fft=window_size, hop_length=hop_size, 
            win_length=window_size, window=window, center=center, pad_mode=pad_mode, 
            freeze_parameters=True)

        # Logmel feature extractor
        self.logmel_extractor = LogmelFilterBank(sr=sample_rate, n_fft=window_size, 
            n_mels=mel_bins, fmin=fmin, fmax=fmax, ref=ref, amin=amin, top_db=top_db, 
            freeze_parameters=True)

        # Spec augmenter
        self.spec_augmenter = SpecAugmentation(time_drop_width=64, time_stripes_num=2, 
            freq_drop_width=8, freq_stripes_num=2)

        self.bn0 = torch.nn.BatchNorm2d(64)

        # activaction function
        self.act = torch.nn.ReLU(inplace=False)
        self.loss_function = torch.nn.BCELoss()
        self.num_frames = int(self.max_duration/self.frame_length)
        # Transfer to another task layer
        self.fc_transfer = torch.nn.Linear(2048, 1024, bias=False)
        self.bn_fc_transfer = torch.nn.BatchNorm1d(1024)
        self.final = torch.nn.Linear(1024, self.num_classes * self.num_frames)
        # Compute Segment-Based Statistics
        self.segm_based_stats_val = SEDSegmentBasedMetrics(num_classes = self.num_classes, threshold=0.9)
        self.init_weights()

    def init_weights(self):
        init_layer(self.fc_transfer)
        init_layer(self.final)
        init_bn(self.bn_fc_transfer)

    def forward(self, input):
        """Input: (batch_size, data_length)"""
        batch_size = input.shape[0]
        # Log mel spectrogram
        x = self.spectrogram_extractor(input)   # (batch_size, 1, time_steps, freq_bins)
        x = self.logmel_extractor(x)    # (batch_size, 1, time_steps, mel_bins)
        
        x = x.transpose(1, 3)
        x = self.bn0(x)
        x = x.transpose(1, 3)

        if self.training:
            x = self.spec_augmenter(x)
        # vgg expects rgb images -> repeat the spectrogram three times
        x = torch.cat((x, x, x), dim = 1)
        embedding = self.base(x)
        embedding = F.dropout(self.act(self.bn_fc_transfer(self.fc_transfer(embedding))), p = 0.2, training = self.training)
        embedding = F.dropout(self.final(embedding), p = 0.2, training = self.training)
        embedding = torch.sigmoid(embedding.reshape((batch_size, 1, self.num_classes, self.num_frames)))
        embedding = embedding.squeeze(1)
        return embedding

    def training_step(self, batch, batch_idx):
        audio_features , batch_labels, idx = batch
        out = self(audio_features.squeeze())
        loss = self.loss(out, batch_labels)
        self.log('train/loss', loss)
        return loss

    def validation_step(self, batch, batch_idx):
        audio_features , batch_labels, idx = batch
        out = self(audio_features.squeeze())
        loss = self.loss(out, batch_labels)
        self.log('val/loss', loss, on_epoch = True, prog_bar = True)
        self.segm_based_stats_val.update(out, batch_labels)
        return loss
    
    def validation_epoch_end(self, val_all):
        _, precision_classwise, recall_classwise, fscore_classwise, fscore_general, ER, accuracy, specificity, acc_mir = self.segm_based_stats_val.compute()
        self.log('val/precision_class', torch.mean(precision_classwise))
        self.log('val/recall_class', torch.mean(recall_classwise))
        self.log('val/fscore_class', fscore_classwise)
        self.log('val/fscore_general', fscore_general)
        self.log('val/ER', ER)
        self.log('val/accuracy', accuracy)
        self.log('val/specificity', specificity)
        self.log('val/acc_mir', acc_mir)
        for i in range(recall_classwise.shape[0]):
            self.log("val/recall_{}".format(i+1), recall_classwise[i])
        self.segm_based_stats_val.reset()
        

    def configure_optimizers(self):
        optim =  torch.optim.AdamW(self.parameters(), lr = self.lr)
        return {
            "optimizer": optim,
            "lr_scheduler": {
                "scheduler": torch.optim.lr_scheduler.ReduceLROnPlateau(optim, verbose=True, patience = 4),
                "monitor": "val/loss",
                "frequency": 1
            },
     }

    def loss(self, out, label):
        return self.loss_function(out , label)

class MobileNetV2_sed(LightningModule):
    def __init__(self, sample_rate, window_size, hop_size, mel_bins, fmin, 
        fmax, lr = 0.001, max_duration = 4, num_classes = 4, frame_length = 0.05):
        super(MobileNetV2_sed, self).__init__()
        # Changing head of VGG
        self.base = torchvision.models.mobilenet_v2()
        self.base.classifier[1] = torch.nn.Linear(in_features = 1280, out_features = 2048)
        self.num_classes = num_classes
        self.frame_length = frame_length
        self.max_duration = max_duration
        self.lr = lr

        window = 'hann'
        center = True
        pad_mode = 'reflect'
        ref = 1.0
        amin = 1e-10
        top_db = None

        # Spectrogram extractor
        self.spectrogram_extractor = Spectrogram(n_fft=window_size, hop_length=hop_size, 
            win_length=window_size, window=window, center=center, pad_mode=pad_mode, 
            freeze_parameters=True)

        # Logmel feature extractor
        self.logmel_extractor = LogmelFilterBank(sr=sample_rate, n_fft=window_size, 
            n_mels=mel_bins, fmin=fmin, fmax=fmax, ref=ref, amin=amin, top_db=top_db, 
            freeze_parameters=True)

        # Spec augmenter
        self.spec_augmenter = SpecAugmentation(time_drop_width=64, time_stripes_num=2, 
            freq_drop_width=8, freq_stripes_num=2)

        self.bn0 = torch.nn.BatchNorm2d(64)

        # activaction function
        self.act = torch.nn.ReLU(inplace=False)
        self.loss_function = torch.nn.BCELoss()
        self.num_frames = int(self.max_duration/self.frame_length)
        # Transfer to another task layer
        self.fc_transfer = torch.nn.Linear(2048, 1024, bias=False)
        self.bn_fc_transfer = torch.nn.BatchNorm1d(1024)
        self.final = torch.nn.Linear(1024, self.num_classes * self.num_frames)
        # Compute Segment-Based Statistics
        self.segm_based_stats_val = SEDSegmentBasedMetrics(num_classes = self.num_classes, threshold=0.9)
        self.init_weights()

    def init_weights(self):
        init_layer(self.fc_transfer)
        init_layer(self.final)
        init_bn(self.bn_fc_transfer)

    def forward(self, input):
        """Input: (batch_size, data_length)"""
        batch_size = input.shape[0]
        # Log mel spectrogram
        x = self.spectrogram_extractor(input)   # (batch_size, 1, time_steps, freq_bins)
        x = self.logmel_extractor(x)    # (batch_size, 1, time_steps, mel_bins)
        
        x = x.transpose(1, 3)
        x = self.bn0(x)
        x = x.transpose(1, 3)

        if self.training:
            x = self.spec_augmenter(x)
        # vgg expects rgb images -> repeat the spectrogram three times
        x = torch.cat((x, x, x), dim = 1)
        embedding = self.base(x)
        embedding = F.dropout(self.act(self.bn_fc_transfer(self.fc_transfer(embedding))), p = 0.2, training = self.training)
        embedding = F.dropout(self.final(embedding), p = 0.2, training = self.training)
        embedding = torch.sigmoid(embedding.reshape((batch_size, 1, self.num_classes, self.num_frames)))
        embedding = embedding.squeeze(1)
        return embedding

    def training_step(self, batch, batch_idx):
        audio_features , batch_labels, idx = batch
        out = self(audio_features.squeeze())
        loss = self.loss(out, batch_labels)
        self.log('train/loss', loss)
        return loss

    def validation_step(self, batch, batch_idx):
        audio_features , batch_labels, idx = batch
        out = self(audio_features.squeeze())
        loss = self.loss(out, batch_labels)
        self.log('val/loss', loss, on_epoch = True, prog_bar = True)
        self.segm_based_stats_val.update(out, batch_labels)
        return loss
    
    def validation_epoch_end(self, val_all):
        _, precision_classwise, recall_classwise, fscore_classwise, fscore_general, ER, accuracy, specificity, acc_mir = self.segm_based_stats_val.compute()
        self.log('val/precision_class', torch.mean(precision_classwise))
        self.log('val/recall_class', torch.mean(recall_classwise))
        self.log('val/fscore_class', fscore_classwise)
        self.log('val/fscore_general', fscore_general)
        self.log('val/ER', ER)
        self.log('val/accuracy', accuracy)
        self.log('val/specificity', specificity)
        self.log('val/acc_mir', acc_mir)
        for i in range(recall_classwise.shape[0]):
            self.log("val/recall_{}".format(i+1), recall_classwise[i])
        self.segm_based_stats_val.reset()
        

    def configure_optimizers(self):
        optim =  torch.optim.AdamW(self.parameters(), lr = self.lr)
        return {
            "optimizer": optim,
            "lr_scheduler": {
                "scheduler": torch.optim.lr_scheduler.ReduceLROnPlateau(optim, verbose=True, patience = 4),
                "monitor": "val/loss",
                "frequency": 1
            },
     }

    def loss(self, out, label):
        return self.loss_function(out , label)

class MobileNetV3_sed(LightningModule):
    def __init__(self, sample_rate, window_size, hop_size, mel_bins, fmin, 
        fmax, lr = 0.001, max_duration = 4, num_classes = 4, frame_length = 0.05):
        super(MobileNetV3_sed, self).__init__()
        # Changing head of VGG
        self.base = torchvision.models.mobilenet_v3_large()
        self.base.classifier[3] = torch.nn.Linear(in_features = 1280, out_features = 2048)
        self.num_classes = num_classes
        self.frame_length = frame_length
        self.max_duration = max_duration
        self.lr = lr

        window = 'hann'
        center = True
        pad_mode = 'reflect'
        ref = 1.0
        amin = 1e-10
        top_db = None

        # Spectrogram extractor
        self.spectrogram_extractor = Spectrogram(n_fft=window_size, hop_length=hop_size, 
            win_length=window_size, window=window, center=center, pad_mode=pad_mode, 
            freeze_parameters=True)

        # Logmel feature extractor
        self.logmel_extractor = LogmelFilterBank(sr=sample_rate, n_fft=window_size, 
            n_mels=mel_bins, fmin=fmin, fmax=fmax, ref=ref, amin=amin, top_db=top_db, 
            freeze_parameters=True)

        # Spec augmenter
        self.spec_augmenter = SpecAugmentation(time_drop_width=64, time_stripes_num=2, 
            freq_drop_width=8, freq_stripes_num=2)

        self.bn0 = torch.nn.BatchNorm2d(64)

        # activaction function
        self.act = torch.nn.ReLU(inplace=False)
        self.loss_function = torch.nn.BCELoss()
        self.num_frames = int(self.max_duration/self.frame_length)
        # Transfer to another task layer
        self.fc_transfer = torch.nn.Linear(2048, 1024, bias=False)
        self.bn_fc_transfer = torch.nn.BatchNorm1d(1024)
        self.final = torch.nn.Linear(1024, self.num_classes * self.num_frames)
        # Compute Segment-Based Statistics
        self.segm_based_stats_val = SEDSegmentBasedMetrics(num_classes = self.num_classes, threshold=0.9)
        self.init_weights()

    def init_weights(self):
        init_layer(self.fc_transfer)
        init_layer(self.final)
        init_bn(self.bn_fc_transfer)

    def forward(self, input):
        """Input: (batch_size, data_length)"""
        batch_size = input.shape[0]
        # Log mel spectrogram
        x = self.spectrogram_extractor(input)   # (batch_size, 1, time_steps, freq_bins)
        x = self.logmel_extractor(x)    # (batch_size, 1, time_steps, mel_bins)
        
        x = x.transpose(1, 3)
        x = self.bn0(x)
        x = x.transpose(1, 3)

        if self.training:
            x = self.spec_augmenter(x)
        # vgg expects rgb images -> repeat the spectrogram three times
        x = torch.cat((x, x, x), dim = 1)
        embedding = self.base(x)
        embedding = F.dropout(self.act(self.bn_fc_transfer(self.fc_transfer(embedding))), p = 0.2, training = self.training)
        embedding = F.dropout(self.final(embedding), p = 0.2, training = self.training)
        embedding = torch.sigmoid(embedding.reshape((batch_size, 1, self.num_classes, self.num_frames)))
        embedding = embedding.squeeze(1)
        return embedding

    def training_step(self, batch, batch_idx):
        audio_features , batch_labels, idx = batch
        out = self(audio_features.squeeze())
        loss = self.loss(out, batch_labels)
        self.log('train/loss', loss)
        return loss

    def validation_step(self, batch, batch_idx):
        audio_features , batch_labels, idx = batch
        out = self(audio_features.squeeze())
        loss = self.loss(out, batch_labels)
        self.log('val/loss', loss, on_epoch = True, prog_bar = True)
        self.segm_based_stats_val.update(out, batch_labels)
        return loss
    
    def validation_epoch_end(self, val_all):
        _, precision_classwise, recall_classwise, fscore_classwise, fscore_general, ER, accuracy, specificity, acc_mir = self.segm_based_stats_val.compute()
        self.log('val/precision_class', torch.mean(precision_classwise))
        self.log('val/recall_class', torch.mean(recall_classwise))
        self.log('val/fscore_class', fscore_classwise)
        self.log('val/fscore_general', fscore_general)
        self.log('val/ER', ER)
        self.log('val/accuracy', accuracy)
        self.log('val/specificity', specificity)
        self.log('val/acc_mir', acc_mir)
        for i in range(recall_classwise.shape[0]):
            self.log("val/recall_{}".format(i+1), recall_classwise[i])
        self.segm_based_stats_val.reset()
        

    def configure_optimizers(self):
        optim =  torch.optim.AdamW(self.parameters(), lr = self.lr)
        return {
            "optimizer": optim,
            "lr_scheduler": {
                "scheduler": torch.optim.lr_scheduler.ReduceLROnPlateau(optim, verbose=True, patience = 4),
                "monitor": "val/loss",
                "frequency": 1
            },
     }

    def loss(self, out, label):
        return self.loss_function(out , label)

class DenseNet121_sed(LightningModule):
    def __init__(self, sample_rate, window_size, hop_size, mel_bins, fmin, 
        fmax, lr = 0.001, max_duration = 4, num_classes = 4, frame_length = 0.05):
        super(DenseNet121_sed, self).__init__()
        # Changing head of VGG
        self.base = torchvision.models.densenet121()
        self.base.classifier = torch.nn.Linear(1024, 2048)
        self.num_classes = num_classes
        self.frame_length = frame_length
        self.max_duration = max_duration
        self.lr = lr

        window = 'hann'
        center = True
        pad_mode = 'reflect'
        ref = 1.0
        amin = 1e-10
        top_db = None

        # Spectrogram extractor
        self.spectrogram_extractor = Spectrogram(n_fft=window_size, hop_length=hop_size, 
            win_length=window_size, window=window, center=center, pad_mode=pad_mode, 
            freeze_parameters=True)

        # Logmel feature extractor
        self.logmel_extractor = LogmelFilterBank(sr=sample_rate, n_fft=window_size, 
            n_mels=mel_bins, fmin=fmin, fmax=fmax, ref=ref, amin=amin, top_db=top_db, 
            freeze_parameters=True)

        # Spec augmenter
        self.spec_augmenter = SpecAugmentation(time_drop_width=64, time_stripes_num=2, 
            freq_drop_width=8, freq_stripes_num=2)

        self.bn0 = torch.nn.BatchNorm2d(64)

        # activaction function
        self.act = torch.nn.ReLU(inplace=False)
        self.loss_function = torch.nn.BCELoss()
        self.num_frames = int(self.max_duration/self.frame_length)
        # Transfer to another task layer
        self.fc_transfer = torch.nn.Linear(2048, 1024, bias=False)
        self.bn_fc_transfer = torch.nn.BatchNorm1d(1024)
        self.final = torch.nn.Linear(1024, self.num_classes * self.num_frames)
        # Compute Segment-Based Statistics
        self.segm_based_stats_val = SEDSegmentBasedMetrics(num_classes = self.num_classes, threshold=0.9)
        self.init_weights()

    def init_weights(self):
        init_layer(self.fc_transfer)
        init_layer(self.final)
        init_bn(self.bn_fc_transfer)

    def forward(self, input):
        """Input: (batch_size, data_length)"""
        batch_size = input.shape[0]
        # Log mel spectrogram
        x = self.spectrogram_extractor(input)   # (batch_size, 1, time_steps, freq_bins)
        x = self.logmel_extractor(x)    # (batch_size, 1, time_steps, mel_bins)
        
        x = x.transpose(1, 3)
        x = self.bn0(x)
        x = x.transpose(1, 3)

        if self.training:
            x = self.spec_augmenter(x)
        # vgg expects rgb images -> repeat the spectrogram three times
        x = torch.cat((x, x, x), dim = 1)
        embedding = self.base(x)
        embedding = F.dropout(self.act(self.bn_fc_transfer(self.fc_transfer(embedding))), p = 0.2, training = self.training)
        embedding = F.dropout(self.final(embedding), p = 0.2, training = self.training)
        embedding = torch.sigmoid(embedding.reshape((batch_size, 1, self.num_classes, self.num_frames)))
        embedding = embedding.squeeze(1)
        return embedding

    def training_step(self, batch, batch_idx):
        audio_features , batch_labels, idx = batch
        out = self(audio_features.squeeze())
        loss = self.loss(out, batch_labels)
        self.log('train/loss', loss)
        return loss

    def validation_step(self, batch, batch_idx):
        audio_features , batch_labels, idx = batch
        out = self(audio_features.squeeze())
        loss = self.loss(out, batch_labels)
        self.log('val/loss', loss, on_epoch = True, prog_bar = True)
        self.segm_based_stats_val.update(out, batch_labels)
        return loss
    
    def validation_epoch_end(self, val_all):
        _, precision_classwise, recall_classwise, fscore_classwise, fscore_general, ER, accuracy, specificity, acc_mir = self.segm_based_stats_val.compute()
        self.log('val/precision_class', torch.mean(precision_classwise))
        self.log('val/recall_class', torch.mean(recall_classwise))
        self.log('val/fscore_class', fscore_classwise)
        self.log('val/fscore_general', fscore_general)
        self.log('val/ER', ER)
        self.log('val/accuracy', accuracy)
        self.log('val/specificity', specificity)
        self.log('val/acc_mir', acc_mir)
        for i in range(recall_classwise.shape[0]):
            self.log("val/recall_{}".format(i+1), recall_classwise[i])
        self.segm_based_stats_val.reset()
        

    def configure_optimizers(self):
        optim =  torch.optim.AdamW(self.parameters(), lr = self.lr)
        return {
            "optimizer": optim,
            "lr_scheduler": {
                "scheduler": torch.optim.lr_scheduler.ReduceLROnPlateau(optim, verbose=True, patience = 4),
                "monitor": "val/loss",
                "frequency": 1
            },
     }

    def loss(self, out, label):
        return self.loss_function(out , label)

class DenseNet169_sed(LightningModule):
    def __init__(self, sample_rate, window_size, hop_size, mel_bins, fmin, 
        fmax, lr = 0.001, max_duration = 4, num_classes = 4, frame_length = 0.05):
        super(DenseNet169_sed, self).__init__()
        # Changing head of VGG
        self.base = torchvision.models.densenet169()
        self.base.classifier = torch.nn.Linear(1664, 2048)
        self.num_classes = num_classes
        self.frame_length = frame_length
        self.max_duration = max_duration
        self.lr = lr

        window = 'hann'
        center = True
        pad_mode = 'reflect'
        ref = 1.0
        amin = 1e-10
        top_db = None

        # Spectrogram extractor
        self.spectrogram_extractor = Spectrogram(n_fft=window_size, hop_length=hop_size, 
            win_length=window_size, window=window, center=center, pad_mode=pad_mode, 
            freeze_parameters=True)

        # Logmel feature extractor
        self.logmel_extractor = LogmelFilterBank(sr=sample_rate, n_fft=window_size, 
            n_mels=mel_bins, fmin=fmin, fmax=fmax, ref=ref, amin=amin, top_db=top_db, 
            freeze_parameters=True)

        # Spec augmenter
        self.spec_augmenter = SpecAugmentation(time_drop_width=64, time_stripes_num=2, 
            freq_drop_width=8, freq_stripes_num=2)

        self.bn0 = torch.nn.BatchNorm2d(64)

        # activaction function
        self.act = torch.nn.ReLU(inplace=False)
        self.loss_function = torch.nn.BCELoss()
        self.num_frames = int(self.max_duration/self.frame_length)
        # Transfer to another task layer
        self.fc_transfer = torch.nn.Linear(2048, 1024, bias=False)
        self.bn_fc_transfer = torch.nn.BatchNorm1d(1024)
        self.final = torch.nn.Linear(1024, self.num_classes * self.num_frames)
        # Compute Segment-Based Statistics
        self.segm_based_stats_val = SEDSegmentBasedMetrics(num_classes = self.num_classes, threshold=0.9)
        self.init_weights()

    def init_weights(self):
        init_layer(self.fc_transfer)
        init_layer(self.final)
        init_bn(self.bn_fc_transfer)

    def forward(self, input):
        """Input: (batch_size, data_length)"""
        batch_size = input.shape[0]
        # Log mel spectrogram
        x = self.spectrogram_extractor(input)   # (batch_size, 1, time_steps, freq_bins)
        x = self.logmel_extractor(x)    # (batch_size, 1, time_steps, mel_bins)
        
        x = x.transpose(1, 3)
        x = self.bn0(x)
        x = x.transpose(1, 3)

        if self.training:
            x = self.spec_augmenter(x)
        # vgg expects rgb images -> repeat the spectrogram three times
        x = torch.cat((x, x, x), dim = 1)
        embedding = self.base(x)
        embedding = F.dropout(self.act(self.bn_fc_transfer(self.fc_transfer(embedding))), p = 0.2, training = self.training)
        embedding = F.dropout(self.final(embedding), p = 0.2, training = self.training)
        embedding = torch.sigmoid(embedding.reshape((batch_size, 1, self.num_classes, self.num_frames)))
        embedding = embedding.squeeze(1)
        return embedding

    def training_step(self, batch, batch_idx):
        audio_features , batch_labels, idx = batch
        out = self(audio_features.squeeze())
        loss = self.loss(out, batch_labels)
        self.log('train/loss', loss)
        return loss

    def validation_step(self, batch, batch_idx):
        audio_features , batch_labels, idx = batch
        out = self(audio_features.squeeze())
        loss = self.loss(out, batch_labels)
        self.log('val/loss', loss, on_epoch = True, prog_bar = True)
        self.segm_based_stats_val.update(out, batch_labels)
        return loss
    
    def validation_epoch_end(self, val_all):
        _, precision_classwise, recall_classwise, fscore_classwise, fscore_general, ER, accuracy, specificity, acc_mir = self.segm_based_stats_val.compute()
        self.log('val/precision_class', torch.mean(precision_classwise))
        self.log('val/recall_class', torch.mean(recall_classwise))
        self.log('val/fscore_class', fscore_classwise)
        self.log('val/fscore_general', fscore_general)
        self.log('val/ER', ER)
        self.log('val/accuracy', accuracy)
        self.log('val/specificity', specificity)
        self.log('val/acc_mir', acc_mir)
        for i in range(recall_classwise.shape[0]):
            self.log("val/recall_{}".format(i+1), recall_classwise[i])
        self.segm_based_stats_val.reset()
        

    def configure_optimizers(self):
        optim =  torch.optim.AdamW(self.parameters(), lr = self.lr)
        return {
            "optimizer": optim,
            "lr_scheduler": {
                "scheduler": torch.optim.lr_scheduler.ReduceLROnPlateau(optim, verbose=True, patience = 4),
                "monitor": "val/loss",
                "frequency": 1
            },
     }

    def loss(self, out, label):
        return self.loss_function(out , label)

###################### SED MODELS
class Transfer_CNN14(LightningModule):

    def __init__(self, sample_rate, window_size, hop_size, mel_bins, fmin, 
        fmax, lr = 0.001, max_duration = 4, num_classes = 4, frame_length = 0.05, check_point = None, freeze_base = False):
        """Classifier for a new task using pretrained Cnn14 as a sub module.
        """
        super(Transfer_CNN14, self).__init__()
        self.base = Cnn14(sample_rate, window_size, hop_size, mel_bins, fmin, 
            fmax, 527)

        self.num_classes = num_classes
        self.frame_length = frame_length
        self.max_duration = max_duration
        self.lr = lr

        if check_point is not None: # Pretrained on AudioSet
            self.load_pretrain(check_point)
            if freeze_base:
                # Freeze AudioSet pretrained layers
                for param in self.base.parameters():
                    param.requires_grad = False

        


        # activaction function
        self.act = torch.nn.ReLU(inplace=False)
        self.loss_function = torch.nn.BCELoss()
        self.num_frames = int(self.max_duration/self.frame_length)
        # Transfer to another task layer
        self.fc_transfer = torch.nn.Linear(2048, 1024, bias=False)
        self.bn_fc_transfer = torch.nn.BatchNorm1d(1024)
        self.final = torch.nn.Linear(1024, self.num_classes * self.num_frames)
        # Compute Segment-Based Statistics
        self.segm_based_stats_val = SEDSegmentBasedMetrics(num_classes = self.num_classes, threshold=0.9)
        self.init_weights()

    def init_weights(self):
        init_layer(self.fc_transfer)
        init_layer(self.final)
        init_bn(self.bn_fc_transfer)

    def load_pretrain(self, pretrained_checkpoint_path):
        checkpoint = torch.load(pretrained_checkpoint_path)
        self.base.load_state_dict(checkpoint['model'])

    def forward(self, input, mixup_lambda=None):
        """Input: (batch_size, data_length)
        """
        batch_size = input.shape[0]
        output_dict = self.base(input, mixup_lambda)
        embedding = output_dict['embedding']
        embedding = F.dropout(self.act(self.bn_fc_transfer(self.fc_transfer(embedding))), p = 0.2, training = self.training)
        embedding = F.dropout(self.final(embedding), p = 0.2, training = self.training)
        embedding = torch.sigmoid(embedding.reshape((batch_size, 1, self.num_classes, self.num_frames)))
        embedding = embedding.squeeze(1)
        return embedding

    def training_step(self, batch, batch_idx):
        audio_features , batch_labels, idx = batch
        out = self(audio_features.squeeze())
        loss = self.loss(out, batch_labels)
        self.log('train/loss', loss)
        # self.segm_based_stats_train.update(out, batch_labels)
        return loss

    def validation_step(self, batch, batch_idx):
        audio_features , batch_labels, idx = batch
        out = self(audio_features.squeeze())
        loss = self.loss(out, batch_labels)
        self.log('val/loss', loss, on_epoch = True, prog_bar = True)
        self.segm_based_stats_val.update(out, batch_labels)
        return loss
    
    def validation_epoch_end(self, val_all):
        _, precision_classwise, recall_classwise, fscore_classwise, fscore_general, ER, accuracy, specificity, acc_mir = self.segm_based_stats_val.compute()
        self.log('val/precision_class', torch.mean(precision_classwise))
        self.log('val/recall_class', torch.mean(recall_classwise))
        self.log('val/fscore_class', fscore_classwise)
        self.log('val/fscore_general', fscore_general)
        self.log('val/ER', ER)
        self.log('val/accuracy', accuracy)
        self.log('val/specificity', specificity)
        self.log('val/acc_mir', acc_mir)
        for i in range(recall_classwise.shape[0]):
            self.log("val/recall_{}".format(i+1), recall_classwise[i])
        self.segm_based_stats_val.reset()
        

    def configure_optimizers(self):
        optim =  torch.optim.AdamW(self.parameters(), lr = self.lr)
        return {
            "optimizer": optim,
            "lr_scheduler": {
                "scheduler": torch.optim.lr_scheduler.ReduceLROnPlateau(optim, verbose=True, patience = 4),
                "monitor": "val/loss",
                "frequency": 1
            },
     }

    def loss(self, out, label):
        return self.loss_function(out , label)

'''------------------------------------------------------------------------------------------------------------'''
class WavegramLogMel(LightningModule):
    def __init__(self, sample_rate, window_size, hop_size, mel_bins, fmin, 
        fmax, lr = 0.001, max_duration = 4, num_classes = 4, frame_length = 0.05):
        
        super(WavegramLogMel, self).__init__()

        window = 'hann'
        center = True
        pad_mode = 'reflect'
        ref = 1.0
        amin = 1e-10
        top_db = None

        self.num_classes = num_classes
        self.frame_length = frame_length
        self.max_duration = max_duration
        self.lr = lr

        self.pre_conv0 = torch.nn.Conv1d(in_channels=1, out_channels=64, kernel_size=11, stride=5, padding=5, bias=False)
        self.pre_bn0 = torch.nn.BatchNorm1d(64)
        self.pre_block1 = ConvPreWavBlock(64, 64)
        self.pre_block2 = ConvPreWavBlock(64, 128)
        self.pre_block3 = ConvPreWavBlock(128, 128)
        self.pre_block4 = ConvBlock(in_channels=4, out_channels=64)

        # Spectrogram extractor
        self.spectrogram_extractor = Spectrogram(n_fft=window_size, hop_length=hop_size, 
            win_length=window_size, window=window, center=center, pad_mode=pad_mode, 
            freeze_parameters=True)

        # Logmel feature extractor
        self.logmel_extractor = LogmelFilterBank(sr=sample_rate, n_fft=window_size, 
            n_mels=mel_bins, fmin=fmin, fmax=fmax, ref=ref, amin=amin, top_db=top_db, 
            freeze_parameters=True)

        # Spec augmenter
        self.spec_augmenter = SpecAugmentation(time_drop_width=64, time_stripes_num=2, 
            freq_drop_width=8, freq_stripes_num=2)

        self.bn0 = torch.nn.BatchNorm2d(64)

        self.conv_block1 = ConvBlock(in_channels=1, out_channels=64)
        self.conv_block2 = ConvBlock(in_channels=128, out_channels=128)
        self.conv_block3 = ConvBlock(in_channels=128, out_channels=256)
        self.conv_block4 = ConvBlock(in_channels=256, out_channels=512)
        self.conv_block5 = ConvBlock(in_channels=512, out_channels=1024)
        self.conv_block6 = ConvBlock(in_channels=1024, out_channels=2048)

        self.fc1 = torch.nn.Linear(2048, 2048, bias=True)
        # activaction function
        self.act = torch.nn.ReLU(inplace=False)
        self.loss_function = torch.nn.BCELoss()
        self.num_frames = int(self.max_duration/self.frame_length)
        # Transfer to another task layer
        self.fc_transfer = torch.nn.Linear(2048, 1024, bias=False)
        self.bn_fc_transfer = torch.nn.BatchNorm1d(1024)
        self.final = torch.nn.Linear(1024, self.num_classes * self.num_frames)
        # Compute Segment-Based Statistics
        # self.segm_based_stats_train = SEDSegmentBasedMetrics(num_classes = self.num_classes, threshold=0.9)
        self.segm_based_stats_val = SEDSegmentBasedMetrics(num_classes = self.num_classes, threshold=0.9)
        
        self.init_weight()

    def init_weight(self):
        init_layer(self.pre_conv0)
        init_bn(self.pre_bn0)
        init_bn(self.bn0)
        init_layer(self.fc1)
 
    def forward(self, input, mixup_lambda=None):
        """
        Input: (batch_size, data_length)"""
        batch_size = input.shape[0]
        # Wavegram
        a1 = self.act(self.pre_bn0(self.pre_conv0(input[:, None, :])))
        a1 = self.pre_block1(a1, pool_size=4)
        a1 = self.pre_block2(a1, pool_size=4)
        a1 = self.pre_block3(a1, pool_size=4)
        a1 = a1.reshape((a1.shape[0], -1, 32, a1.shape[-1])).transpose(2, 3)
        a1 = self.pre_block4(a1, pool_size=(1, 1)) # WARNING HERE for TIME Resolution

        # Log mel spectrogram
        x = self.spectrogram_extractor(input)   # (batch_size, 1, time_steps, freq_bins)
        x = self.logmel_extractor(x)    # (batch_size, 1, time_steps, mel_bins)
        
        x = x.transpose(1, 3)
        x = self.bn0(x)
        x = x.transpose(1, 3)

        if self.training:
            x = self.spec_augmenter(x)

        x = self.conv_block1(x, pool_size=(2, 2), pool_type='avg')
        
        # Concatenate Wavegram and Log mel spectrogram along the channel dimension
        x = torch.cat((x, a1), dim=1)

        x = F.dropout(x, p=0.2, training=self.training)
        x = self.conv_block2(x, pool_size=(2, 2), pool_type='avg')
        x = F.dropout(x, p=0.2, training=self.training)
        x = self.conv_block3(x, pool_size=(2, 2), pool_type='avg')
        x = F.dropout(x, p=0.2, training=self.training)
        x = self.conv_block4(x, pool_size=(2, 2), pool_type='avg')
        x = F.dropout(x, p=0.2, training=self.training)
        x = self.conv_block5(x, pool_size=(2, 2), pool_type='avg')
        x = F.dropout(x, p=0.2, training=self.training)
        x = self.conv_block6(x, pool_size=(1, 1), pool_type='avg')
        x = F.dropout(x, p=0.2, training=self.training)
        x = torch.mean(x, dim=3)
        
        (x1, _) = torch.max(x, dim=2)
        x2 = torch.mean(x, dim=2)
        x = x1 + x2
        x = F.dropout(x, p=0.5, training=self.training)
        x = self.act(self.fc1(x))
        embedding = F.dropout(x, p=0.5, training=self.training)
        embedding = F.dropout(self.act(self.bn_fc_transfer(self.fc_transfer(embedding))), p = 0.2, training = self.training)
        embedding = F.dropout(self.final(embedding), p = 0.2, training = self.training)
        embedding = torch.sigmoid(embedding.reshape((batch_size, 1, self.num_classes, self.num_frames)))
        embedding = embedding.squeeze(1)

        return embedding

    def training_step(self, batch, batch_idx):
        audio_features , batch_labels, idx = batch
        out = self(audio_features.squeeze())
        loss = self.loss(out, batch_labels)
        self.log('train/loss', loss)
        return loss

    def validation_step(self, batch, batch_idx):
        audio_features , batch_labels, idx = batch
        out = self(audio_features.squeeze())
        loss = self.loss(out, batch_labels)
        self.log('val/loss', loss, on_epoch = True, prog_bar = True)
        self.segm_based_stats_val.update(out, batch_labels)
        return loss
    
    def validation_epoch_end(self, val_all):
        _, precision_classwise, recall_classwise, fscore_classwise, fscore_general, ER, accuracy, specificity, acc_mir = self.segm_based_stats_val.compute()
        self.log('val/precision_class', torch.mean(precision_classwise))
        self.log('val/recall_class', torch.mean(recall_classwise))
        self.log('val/fscore_class', fscore_classwise)
        self.log('val/fscore_general', fscore_general)
        self.log('val/ER', ER)
        self.log('val/accuracy', accuracy)
        self.log('val/specificity', specificity)
        self.log('val/acc_mir', acc_mir)
        for i in range(recall_classwise.shape[0]):
            self.log("val/recall_{}".format(i+1), recall_classwise[i])
        self.segm_based_stats_val.reset()
        

    def configure_optimizers(self):
        optim =  torch.optim.AdamW(self.parameters(), lr = self.lr)
        return {
            "optimizer": optim,
            "lr_scheduler": {
                "scheduler": torch.optim.lr_scheduler.ReduceLROnPlateau(optim, verbose=True, patience = 4),
                "monitor": "val/loss",
                "frequency": 1
            },
     }

    def loss(self, out, label):
        return self.loss_function(out , label)


###################### MyModel

class AuSPP(LightningModule):
    pass

#### TESTING
from torchinfo import summary
if __name__ == '__main__':
    config = {
            "max_epochs": 20,
            "batch_size": 16,
            "lr": 0.001,
            "window_size": 512,
            "hop_size": 160,
            "mel_bins": 64,
            "frame_length": 0.05,
            "max_duration": 4,
            "num_classes": 4,
            "sr": 16000,
    }
    model = WavegramLogMel(sample_rate = config['sr'], window_size = config['window_size'], hop_size = config['hop_size'],
                           mel_bins = config['mel_bins'], fmin = 50, fmax = 8000, lr = config['lr'], max_duration = config['max_duration'],
                           num_classes = config['num_classes'], frame_length = config['frame_length'])
    test = torch.randn([2, 64000])
    result = model(test)
    print(result.shape)
    summary(model, input_size=(2, 64000))