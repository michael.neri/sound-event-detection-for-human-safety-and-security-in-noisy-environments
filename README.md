# AuSPP model and SEDDOB dataset for Sound Event Detection
Official repository of the article "Sound Event Detection for Human Safety and Security in Noisy Environments".

## Download

The dataset SEDDOB can be downloaded from this [link](https://mega.nz/file/Us9XlRTA#pbKPrGDbm3EFJvPgPhC4uB5sI8ptfpfGY__m-5nyeNA)


## Authors
Michael Neri, Department of Industrial, Electronic and Mechanical Engineering, Roma Tre University, [michael.neri@uniroma3.it](mailto:michael.neri@uniroma3.it)

Federica Battisti, Department of Information Engineering, University of Padova, [federica.battisti@unipd.it](mailto:federica.battisti@uniroma3.it)

Alessandro Neri, Department of Industrial, Electronic and Mechanical Engineering, Roma Tre University, [alessandro.neri@uniroma3.it](mailto:alessandro.neri@uniroma3.it)

Marco Carli, Department of Industrial, Electronic and Mechanical Engineering, Roma Tre University, [marco.carli@uniroma3.it](mailto:marco.carli@uniroma3.it)


## Description
In this contribution, we present a sound anomaly detection system based on a fully convolutional network which exploits image spatial filtering and an Atrous Spatial Pyramid Pooling module. To cope with the lack of datasets specifically designed for sound event detection, a dataset for the specific application of noisy bus environments has been designed. The dataset has been obtained by mixing background audio files, recorded in a real environment, with anomalous events extracted from monophonic collections of labelled audios. The performances of the proposed system have been evaluated through segment-based metrics such as error rate, recall, and F1-Score. Moreover, robustness and precision have been evaluated through four different tests. The analysis of the results shows that the proposed sound event detector outperforms both state-of-the-art methods and general purpose deep learning-solutions.

![AuSPP](./AuSPP.PNG)

## Contacts
Any issues using the scripts and the dataset can be addressed to Michael Neri via mail [michael.neri@uniroma3.it](mailto:michael.neri@uniroma3.it)

## References
If you use these resources, please cite the following article:
```
@article{neri_Access_2022,
title = {{Sound Event Detection for Human Safety and Security in Nosiy Environments}},
author = {Michael Neri and Federica Battisti and Alessandro Neri and Marco Carli},
year = {2022},
journal = {IEEE Access},
doi = {10.1109/ACCESS.2022.3231681}
}
```
